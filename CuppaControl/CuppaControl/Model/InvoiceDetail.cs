﻿using System;

namespace CuppaControl.Model
{
    [Serializable]
    public class InvoiceDetail
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public string Note { get; set; }
    }
}
