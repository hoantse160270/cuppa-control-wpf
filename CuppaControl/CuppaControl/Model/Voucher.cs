﻿using System;

namespace CuppaControl.Model
{
    public class Voucher
    {
        public string Id { get; set; }
        public int TypeDiscount { get; set; }
        public int Value { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
