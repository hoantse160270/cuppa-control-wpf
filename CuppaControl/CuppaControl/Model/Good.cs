﻿namespace CuppaControl.Model
{
    public class Good
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string IsInUser { get; set; }
        public string CategoryId { get; set; }
        public int Quantity { set; get; }
    }
}
