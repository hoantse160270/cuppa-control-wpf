﻿namespace CuppaControl.Model
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
        public string Status { get; set; }
        public int Unit { get; set; }
        public bool OutOfStock { get; set; }
        public string ProductImg { get; set; }
        public double Price { get; set; }
    }
}
