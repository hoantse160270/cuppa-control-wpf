﻿namespace CuppaControl.Model
{
    public enum UserGender
    {
        // False
        Female,
        // True
        Male
    }

    public enum Role
    {
        ShopOwner,
        Employee
    }

    public enum VoucherType
    {
        Percent,
        Value,
    }

    public enum PaymentType
    {
        AtStore,
        Online
    }

    #region Fields

    #endregion

    #region Properties

    #endregion

    #region Commands

    #endregion

    #region Constructors

    #endregion

    #region Methods

    #endregion
}
