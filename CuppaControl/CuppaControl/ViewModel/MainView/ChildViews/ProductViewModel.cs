﻿using CuppaControl.Model;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Windows.Input;
using System;
using Flurl;
using CuppaControl.Global;
using System.Windows;

namespace CuppaControl.ViewModel
{
    public class ProductViewModel : ViewModelBase
    {
        #region Fields
        private int pageSize = 10;
        private int _currentPage;
        private int _totalPage;

        private bool _prevButtonEnable;
        private bool _nextButtonEnable;

        private List<string> _sortList;
        private int _sortSelectedIndex;

        private List<string> _statusList;
        private int _statusSelectedIndex;

        private string _searchKeyword;

        private List<ProductCategory> _categories;
        private ProductCategory _selectedCategory;

        private List<Product> _products;
        private Product _selectedProduct;

        private Visibility _notFoundVisibility;
        #endregion

        #region Properties
        public List<string> SortList
        {
            get
            {
                return _sortList;
            }
            set
            {
                _sortList = value;
                OnPropertyChanged(nameof(SortList));
            }
        }
        public int SortSelectedIndex
        {
            get
            {
                return _sortSelectedIndex;
            }
            set
            {
                _sortSelectedIndex = value;
                OnPropertyChanged(nameof(SortSelectedIndex));
            }
        }

        public List<string> StatusList
        {
            get
            {
                return _statusList;
            }
            set
            {
                _statusList = value;
                OnPropertyChanged(nameof(StatusList));
            }
        }
        public int StatusSelectedIndex
        {
            get
            {
                return _statusSelectedIndex;
            }
            set
            {
                _statusSelectedIndex = value;
                OnPropertyChanged(nameof(StatusSelectedIndex));
            }
        }

        public string SearchKeyword
        {
            get
            {
                return _searchKeyword;
            }
            set
            {
                _searchKeyword = value;
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }


        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                _currentPage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
        }
        public int TotalPage
        {
            get
            {
                return _totalPage;
            }
            set
            {
                _totalPage = value;
                OnPropertyChanged(nameof(TotalPage));
            }
        }
        public bool PrevButtonEnable
        {
            get
            {
                return _prevButtonEnable;
            }
            set
            {
                _prevButtonEnable = value;
                OnPropertyChanged(nameof(PrevButtonEnable));
            }
        }
        public bool NextButtonEnable
        {
            get
            {
                return _nextButtonEnable;
            }
            set
            {
                _nextButtonEnable = value;
                OnPropertyChanged(nameof(NextButtonEnable));
            }
        }

        public List<ProductCategory> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public ProductCategory SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                _selectedCategory = value;
                OnPropertyChanged(nameof(SelectedCategory));
            }
        }

        public List<Product> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                CheckPrevNextButtonEnable();
                OnPropertyChanged(nameof(Products));
            }
        }
        public Product SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }

        public Visibility NotFoundVisibility
        {
            get
            {
                return _notFoundVisibility;
            }
            set
            {
                _notFoundVisibility = value;
                OnPropertyChanged(nameof(NotFoundVisibility));
            }
        }
        #endregion

        #region Commands
        public ICommand SearchProductsCommand { get; }
        public ICommand NextProductPageCommand { get; }
        public ICommand PrevProductPageCommand { get; }
        #endregion

        #region Constructors
        public ProductViewModel()
        {
            SortList = new List<string>() { "Default", "Name ASC", "Name DESC", "Price ASC", "Price DESC" };
            StatusList = new List<string>() { "All", "Active", "Inactive" };
            SortSelectedIndex = 0;
            StatusSelectedIndex = 0;

            GetProductCategoryList();
            GetInitialProducts();

            SearchProductsCommand = new ViewModelCommand(ExecuteSearchProductsCommand);
            NextProductPageCommand = new ViewModelCommand(ExecuteNextProductPageCommand);
            PrevProductPageCommand = new ViewModelCommand(ExecutePrevProductPageCommand);
        }

        #endregion

        #region Methods
        private void ExecuteNextProductPageCommand(object obj)
        {
            CurrentPage++;
            LoadProductPage();
        }
        private void ExecutePrevProductPageCommand(object obj)
        {
            CurrentPage--;
            LoadProductPage();
        }
        private async void GetProductCategoryList()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("category")
                    .AppendPathSegment("list")
                    .AllowAnyHttpStatus()
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    List<ProductCategory> cateList = JsonConvert.DeserializeObject<List<ProductCategory>>(result["result"].ToString());
                    ProductCategory cate = new ProductCategory() { Id = 0, Name = "All" };
                    cateList.Add(cate);
                    Categories = cateList;
                    SelectedCategory = Categories[Categories.Count - 1];
                }
                else
                {

                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void GetInitialProducts()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        return;
                    }
                    List<Product> productList = JsonConvert.DeserializeObject<List<Product>>(result["result"]["list"].ToString());
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    CurrentPage = 1;
                    NotFoundVisibility = Visibility.Collapsed;
                    Products = productList;
                    SelectedProduct = null;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void SearchProducts()
        {
            try
            {
                RefreshProducts();
                ProductCategory cate = Categories.Find(c => c.Id == SelectedCategory.Id);
                string cateId = (cate == null || cate.Id == 0) ? string.Empty : cate.Id.ToString();
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("Name", SearchKeyword)
                    .SetQueryParam("Sortname", GetSortName())
                    .SetQueryParam("Status", GetStatus())
                    .SetQueryParam("CategoryId", cateId)
                    .SetQueryParam("Sortprice", GetSortPrice())
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        return;
                    }
                    List<Product> productList = JsonConvert.DeserializeObject<List<Product>>(result["result"]["list"].ToString());
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    CurrentPage = 1;
                    NotFoundVisibility = Visibility.Collapsed;
                    Products = productList;
                    SelectedProduct = null;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    CheckPrevNextButtonEnable();
                    ResetPages();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void LoadProductPage()
        {
            try
            {
                RefreshProducts();
                ProductCategory cate = Categories.Find(c => c.Id == SelectedCategory.Id);
                string cateId = (cate == null || cate.Id == 0) ? string.Empty : cate.Id.ToString();
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("Name", SearchKeyword)
                    .SetQueryParam("Sortname", GetSortName())
                    .SetQueryParam("Status", GetStatus())
                    .SetQueryParam("CategoryId", cateId)
                    .SetQueryParam("Sortprice", GetSortPrice())
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", CurrentPage)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<Product> productList = JsonConvert.DeserializeObject<List<Product>>(result["result"]["list"].ToString());
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    Products = productList;
                    NotFoundVisibility = Visibility.Collapsed;
                    SelectedProduct = null;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private void CheckPrevNextButtonEnable()
        {
            if (Products == null || Products.Count == 0)
            {
                PrevButtonEnable = false;
                NextButtonEnable = false;
                return;
            }
            // Prev Button
            if (CurrentPage == 1 ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                PrevButtonEnable = false;
            }
            else if (CurrentPage > 1 && CurrentPage <= TotalPage)
            {
                PrevButtonEnable = true;
            }
            else
            {
                PrevButtonEnable = false;
            }

            // Next Button
            if (CurrentPage == TotalPage ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                NextButtonEnable = false;
            }
            else if (CurrentPage >= 1 && CurrentPage < TotalPage)
            {
                NextButtonEnable = true;
            }
            else
            {
                NextButtonEnable = false;
            }
        }
        private string GetSortName()
        {
            switch (SortSelectedIndex)
            {
                case 1:
                    return "asc";
                case 2:
                    return "desc";
                default:
                    return string.Empty;
            }
        }
        private string GetSortPrice()
        {
            switch (SortSelectedIndex)
            {
                case 3:
                    return "asc";
                case 4:
                    return "desc";
                default:
                    return string.Empty;
            }
        }
        private string GetStatus()
        {
            switch (StatusSelectedIndex)
            {
                case 1:
                    return "Active";
                case 2:
                    return "Inactive";
                default:
                    return string.Empty;
            }
        }

        private void ExecuteSearchProductsCommand(object obj)
        {
            SearchProducts();
        }

        private void RefreshProducts()
        {
            Products = new List<Product>();
            SelectedProduct = null;
        }

        private void ResetPages()
        {
            CurrentPage = 0;
            TotalPage = 0;
        }
        #endregion
    }
}
