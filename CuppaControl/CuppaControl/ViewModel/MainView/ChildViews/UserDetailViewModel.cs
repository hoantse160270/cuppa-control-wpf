﻿using CuppaControl.Global;

namespace CuppaControl.ViewModel
{
    public class UserDetailViewModel : ViewModelBase
    {
        #region Fields
        private string _avatar;
        private string _fullname;
        private string _dateOfBirth;
        private string _gender;
        private string _phone;
        private string _email;
        private string _address;
        private string _role;
        private string _startedDate;
        private string _expirationDate;
        private string _salary;
        private string _image;

        #endregion

        #region Properties
        public string Avatar
        {
            get
            {
                return _avatar;
            }
            set
            {
                _avatar = value;
                OnPropertyChanged(nameof(Avatar));
            }
        }
        public string Fullname
        {
            get
            {
                return _fullname;
            }
            set
            {
                _fullname = value;
                OnPropertyChanged(nameof(Fullname));
            }
        }
        public string DateOfBirth
        {
            get
            {
                return _dateOfBirth;
            }
            set
            {
                _dateOfBirth = value;
                OnPropertyChanged(nameof(DateOfBirth));
            }
        }
        public string Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
                OnPropertyChanged(nameof(Gender));
            }
        }
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                _phone = value;
                OnPropertyChanged(nameof(Phone));
            }
        }
        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
                OnPropertyChanged(nameof(Email));
            }
        }
        public string Address
        {
            get
            {
                return _address;
            }
            set
            {
                _address = value;
                OnPropertyChanged(nameof(Address));
            }
        }
        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
                OnPropertyChanged(nameof(Role));
            }
        }
        public string StartedDate
        {
            get
            {
                return _startedDate;
            }
            set
            {
                _startedDate = value;
                OnPropertyChanged(nameof(StartedDate));
            }
        }
        public string ExpirationDate
        {
            get
            {
                return _expirationDate;
            }
            set
            {
                _expirationDate = value;
                OnPropertyChanged(nameof(ExpirationDate));
            }
        }
        public string Salary
        {
            get
            {
                return _salary;
            }
            set
            {
                _salary = value;
                OnPropertyChanged(nameof(Salary));
            }
        }
        public string Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                OnPropertyChanged(nameof(Image));
            }
        }
        #endregion

        #region Commands

        #endregion

        #region Constructors
        public UserDetailViewModel()
        {
            AssignData();
        }
        #endregion

        #region Methods
        private void AssignData()
        {
            if(GlobalVarible.User != null)
            {
                Fullname = GlobalVarible.User.FullName;
                DateOfBirth = GlobalVarible.User.DateOfBirth;
                Gender = GlobalVarible.User.Gender;
                Phone = GlobalVarible.User.Phone;
                Email = GlobalVarible.User.Email;
                Address = GlobalVarible.User.Address;
                Role = GlobalVarible.User.Role;
                StartedDate = GlobalVarible.User.StartToWork;
                ExpirationDate = GlobalVarible.User.ContractExpiration;
                Salary = string.Format("{0:0,0.00}", GlobalVarible.User.CofficientsSalary);
                Image = (GlobalVarible.User.Avatar == null) ? "/Images/pepe_the_frog.jpg" : GlobalVarible.User.Avatar;
            }
        }
        #endregion
    }
}
