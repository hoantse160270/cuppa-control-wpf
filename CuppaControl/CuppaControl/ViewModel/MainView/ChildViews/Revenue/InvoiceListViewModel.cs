﻿using CuppaControl.Global;
using CuppaControl.Model;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;

namespace CuppaControl.ViewModel
{
    public class InvoiceListViewModel : ViewModelBase
    {
        #region Fields
        public const int PageSize = 10;
        private List<InvoiceModel> _invoices;
        private InvoiceModel _selectedInvoice;

        private int _currentPage;
        private int _totalPage;

        private bool _prevButtonEnable;
        private bool _nextButtonEnable;
        private DateTime? _selectedDate;
        private DateTime? _selectedDateTo;
        private Visibility _invoiceDetailVisibility;

        private Visibility _notFoundVisibility;
        #endregion

        #region Properties
        public List<InvoiceModel> Invoices
        {
            get
            {
                return _invoices;
            }
            set
            {
                _invoices = value;
                CheckPrevNextButtonEnable();
                OnPropertyChanged(nameof(Invoices));
            }
        }
        public InvoiceModel SelectedInvoice
        {
            get
            {
                return _selectedInvoice;
            }
            set
            {
                _selectedInvoice = value;
                SelectInvoiceDetail();
                OnPropertyChanged(nameof(SelectedInvoice));
            }
        }

        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                _currentPage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
        }
        public int TotalPage
        {
            get
            {
                return _totalPage;
            }
            set
            {
                _totalPage = value;
                OnPropertyChanged(nameof(TotalPage));
            }
        }
        public bool PrevButtonEnable
        {
            get
            {
                return _prevButtonEnable;
            }
            set
            {
                _prevButtonEnable = value;
                OnPropertyChanged(nameof(PrevButtonEnable));
            }
        }
        public bool NextButtonEnable
        {
            get
            {
                return _nextButtonEnable;
            }
            set
            {
                _nextButtonEnable = value;
                OnPropertyChanged(nameof(NextButtonEnable));
            }
        }
        public DateTime? SelectedDate
        {
            get
            {
                return _selectedDate;
            }
            set
            {
                _selectedDate = value;
                OnPropertyChanged(nameof(SelectedDate));
            }
        }

        public DateTime? SelectedDateTo
        {
            get
            {
                return _selectedDateTo;
            }
            set
            {
                _selectedDateTo = value;
                OnPropertyChanged(nameof(SelectedDateTo));
            }
        }
        public Visibility InvoiceDetailVisibility
        {
            get
            {
                return _invoiceDetailVisibility;
            }
            set
            {
                _invoiceDetailVisibility = value;
                OnPropertyChanged(nameof(InvoiceDetailVisibility));
            }
        }

        public Visibility NotFoundVisibility
        {
            get
            {
                return _notFoundVisibility;
            }
            set
            {
                _notFoundVisibility = value;
                OnPropertyChanged(nameof(NotFoundVisibility));
            }
        }
        #endregion

        #region Commands
        public ICommand ShowInvoiceDetailComand { get; }
        public ICommand ResetSelectedDate { get; }
        public ICommand ShowNextPage { get; }
        public ICommand ShowPrevPage { get; }
        public ICommand FilterInvoices { get; }
        public ICommand CloseInvoiceDetail { get; }
        #endregion

        #region Constructors
        public InvoiceListViewModel()
        {
            InvoiceDetailVisibility = Visibility.Collapsed;
            ShowInvoiceDetailComand = new ViewModelCommand(ExecuteShowInvoiceDetailComand);
            ResetSelectedDate = new ViewModelCommand(ExecuteResetSelectedDate);
            ShowNextPage = new ViewModelCommand(ExecuteShowNextPage);
            ShowPrevPage = new ViewModelCommand(ExecuteShowPrevPage);
            FilterInvoices = new ViewModelCommand(ExecuteFilterInvoices);
            CloseInvoiceDetail = new ViewModelCommand(ExecuteCloseInvoiceDetail);

            LoadInitialInvoices();
        }

        #endregion

        private void ExecuteFilterInvoices(object obj)
        {
            FilterInitialInvoices();
        }

        private void ExecuteShowPrevPage(object obj)
        {
            int currentPage = CurrentPage;
            currentPage--;
            if (currentPage > 0)
            {
                CurrentPage = currentPage;
                LoadInvoicePage();
            }
        }

        private void ExecuteShowNextPage(object obj)
        {
            int currentPage = CurrentPage;
            currentPage++;
            if (currentPage <= TotalPage)
            {
                CurrentPage = currentPage;
                LoadInvoicePage();
            }
        }

        #region Methods
        private void ExecuteResetSelectedDate(object obj)
        {
            SelectedDate = null;
            SelectedDateTo = null;
        }
        private void ExecuteShowInvoiceDetailComand(object obj)
        {
            throw new NotImplementedException();
        }

        private async void FilterInitialInvoices()
        {
            try
            {
                RefreshInvoices();
                var date = SelectedDate.ToString();
                var dateTo = SelectedDateTo.ToString();
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("invoice")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("beginDate", date)
                    .SetQueryParam("endDate", dateTo)
                    .SetQueryParam("pageSize", PageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<InvoiceModel> list = JsonConvert.DeserializeObject<List<InvoiceModel>>(result["result"]["list"].ToString());
                    foreach (var item in list)
                    {
                        item.Total = Math.Round(item.Total, 2);
                    }
                    int pageTotal = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    Invoices.Clear();
                    Invoices = list;
                    TotalPage = pageTotal;
                    CurrentPage = 1;
                    CheckNotFoundVisibility();
                    CheckPrevNextButtonEnable();
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async void LoadInitialInvoices()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("invoice")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("beginDate", (SelectedDate == null) ? "" : SelectedDate.ToString())
                    .SetQueryParam("endDate", (SelectedDateTo == null) ? "" : SelectedDateTo.ToString())
                    .SetQueryParam("pageSize", PageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<InvoiceModel> list = JsonConvert.DeserializeObject<List<InvoiceModel>>(result["result"]["list"].ToString());
                    foreach (var item in list)
                    {
                        item.Total = Math.Round(item.Total, 2);
                    }
                    int pageTotal = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    Invoices = list;
                    TotalPage = pageTotal;
                    CurrentPage = 1;
                    CheckNotFoundVisibility();
                    CheckPrevNextButtonEnable();
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private async void LoadInvoicePage()
        {
            try
            {
                RefreshInvoices();
                var response = await "http://localhost:5108/api/invoice/filter"
                    .AllowAnyHttpStatus()
                    .SetQueryParam("beginDate", (SelectedDate == null) ? "" : SelectedDate.ToString())
                    .SetQueryParam("endDate", (SelectedDateTo == null) ? "" : SelectedDateTo.ToString())
                    .SetQueryParam("pageSize", PageSize)
                    .SetQueryParam("pageNumber", CurrentPage)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<InvoiceModel> list = JsonConvert.DeserializeObject<List<InvoiceModel>>(result["result"]["list"].ToString());
                    foreach (var item in list)
                    {
                        item.Total = Math.Round(item.Total, 2);
                    }
                    int pageTotal = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    Invoices.Clear();
                    Invoices = list;
                    TotalPage = pageTotal;
                    CheckNotFoundVisibility();
                    CheckPrevNextButtonEnable();
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void CheckPrevNextButtonEnable()
        {
            if (Invoices == null || Invoices.Count == 0)
            {
                PrevButtonEnable = false;
                NextButtonEnable = false;
                return;
            }
            // Prev Button
            if (CurrentPage == 1 ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                PrevButtonEnable = false;
            }
            else if (CurrentPage > 1 && CurrentPage <= TotalPage)
            {
                PrevButtonEnable = true;
            }
            else
            {
                PrevButtonEnable = false;
            }

            // Next Button
            if (CurrentPage == TotalPage ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                NextButtonEnable = false;
            }
            else if (CurrentPage >= 1 && CurrentPage < TotalPage)
            {
                NextButtonEnable = true;
            }
            else
            {
                NextButtonEnable = false;
            }
        }
        private void SelectInvoiceDetail()
        {
            if (SelectedInvoice != null)
            {
                InvoiceDetailVisibility = Visibility.Visible;
            }
        }

        private void ExecuteCloseInvoiceDetail(object obj)
        {
            InvoiceDetailVisibility |= Visibility.Collapsed;
            SelectedInvoice = null;
        }


        private void RefreshInvoices()
        {
            Invoices = new List<InvoiceModel>();
            SelectedInvoice = null;
        }

        private void ResetPages()
        {
            TotalPage = 0;
            CurrentPage = 0;
        }

        private void CheckNotFoundVisibility()
        {
            if (Invoices == null || Invoices.Count == 0)
            {
                NotFoundVisibility = Visibility.Visible;
            }
            else
            {
                NotFoundVisibility = Visibility.Collapsed;
            }
        }
        #endregion
    }
}
