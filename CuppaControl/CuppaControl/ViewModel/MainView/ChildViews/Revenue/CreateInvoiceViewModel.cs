﻿using CuppaControl.Global;
using CuppaControl.Model;
using Flurl;
using Flurl.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace CuppaControl.ViewModel
{
    public class CreateInvoiceViewModel : ViewModelBase
    {
        #region Fields
        private List<InvoiceProduct> _invoiceProducts;
        private List<ProductCategory> _categories;
        private List<CategoryProduct> _products;
        private List<Voucher> _vouchers;

        private InvoiceProduct _selectedInvoiceProduct;
        private ProductCategory _selectedCategory;
        private CategoryProduct _selectedProduct;
        private Voucher _selectedVoucher;

        private double _productPrice;
        private double _voucherDiscount;
        private double _tax;
        private double _totalPrice;
        #endregion

        #region Properties
        public List<InvoiceProduct> InvoiceProducts
        {
            get
            {
                return _invoiceProducts;
            }
            set
            {
                _invoiceProducts = value;
                OnPropertyChanged(nameof(InvoiceProducts));
            }
        }
        public List<ProductCategory> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }
        public List<CategoryProduct> Products
        {
            get
            {
                return _products;
            }
            set
            {
                _products = value;
                OnPropertyChanged(nameof(Products));
            }
        }
        public List<Voucher> Vouchers
        {
            get
            {
                return _vouchers;
            }
            set
            {
                _vouchers = value;
                OnPropertyChanged(nameof(Vouchers));
            }
        }
        public CategoryProduct SelectedProduct
        {
            get
            {
                return _selectedProduct;
            }
            set
            {
                _selectedProduct = value;
                if (_selectedProduct != null)
                {
                    AddProductToInvoice(SelectedProduct);
                }
                OnPropertyChanged(nameof(SelectedProduct));
            }
        }
        public InvoiceProduct SelectedInvoiceProduct
        {
            get
            {
                return _selectedInvoiceProduct;
            }
            set
            {
                _selectedInvoiceProduct = value;
                OnPropertyChanged(nameof(SelectedInvoiceProduct));
            }
        }
        public ProductCategory SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                _selectedCategory = value;
                UpdateProducts();
                OnPropertyChanged(nameof(SelectedCategory));
            }
        }
        public Voucher SelectedVoucher
        {
            get
            {
                return _selectedVoucher;
            }
            set
            {
                _selectedVoucher = value;
                OnPropertyChanged(nameof(SelectedVoucher));
            }
        }

        public double ProductPrice
        {
            get
            {
                return _productPrice;
            }
            set
            {
                _productPrice = value;
                OnPropertyChanged(nameof(ProductPrice));
            }
        }
        public double VoucherDiscount
        {
            get
            {
                return _voucherDiscount;
            }
            set
            {
                _voucherDiscount = value;
                OnPropertyChanged(nameof(VoucherDiscount));
            }
        }
        public double Tax
        {
            get
            {
                return _tax;
            }
            set
            {
                _tax = value;
                OnPropertyChanged(nameof(Tax));
            }
        }
        public double TotalPrice
        {
            get
            {
                return _totalPrice;
            }
            set
            {
                _totalPrice = value;
                OnPropertyChanged(nameof(TotalPrice));
            }
        }
        #endregion

        #region Commands
        public ICommand RemoveInvoiceProductCommand { get; }
        public ICommand CancelCreatingInvoiceCommand { get; }
        public ICommand ConfirmCreatingInvoiceCommand { get; }
        public ICommand IncreaseProductCommand { get; }
        public ICommand DecreaseProductCommand { get; }

        #endregion

        #region Constructors
        public CreateInvoiceViewModel()
        {
            RemoveInvoiceProductCommand = new ViewModelCommand(ExecuteRemoveInvoiceProductCommand);
            CancelCreatingInvoiceCommand = new ViewModelCommand(ExecuteCancelCreatingInvoiceCommand);
            ConfirmCreatingInvoiceCommand = new ViewModelCommand(ExecuteConfirmCreatingInvoiceCommand);
            IncreaseProductCommand = new ViewModelCommand(ExecuteIncreaseProductCommand);
            DecreaseProductCommand = new ViewModelCommand(ExecuteDecreaseProductCommand);

            ProductPrice = 0;
            VoucherDiscount = 0;
            Tax = 0;
            TotalPrice = 0;

            //SetInvoiceProducts();
            //SetCategoriesList();
            InvoiceProducts = new List<InvoiceProduct>();
            GetProductCategoryList();
            //SetProducts();
        }

        #endregion

        #region Methods
        private async void GetProductCategoryList()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("category")
                    .AppendPathSegment("list")
                    .AllowAnyHttpStatus()
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        return;
                    }
                    List<ProductCategory> cateList = JsonConvert.DeserializeObject<List<ProductCategory>>(result["result"].ToString());
                    Categories = cateList;
                    SelectedCategory = Categories[0];
                }
                else
                {

                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void GetVouchersList()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("voucher")
                    .AllowAnyHttpStatus()
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        return;
                    }
                    List<Voucher> voucherList = JsonConvert.DeserializeObject<List<Voucher>>(result["result"].ToString());
                    Vouchers = voucherList;
                }
                else
                {

                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void UpdateProductsByCategory()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("product")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("Status", "ACTIVE")
                    .SetQueryParam("OutOfStock", false)
                    .SetQueryParam("CategoryId", SelectedCategory.Id)
                    .SetQueryParam("pageSize", 100)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        return;
                    }
                    List<CategoryProduct> productList = JsonConvert.DeserializeObject<List<CategoryProduct>>(result["result"]["list"].ToString());
                    Products = productList;
                    SelectedProduct = null;
                }
                else
                {

                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private void UpdateProducts()
        {
            UpdateProductsByCategory();
        }

        private void AddProductToInvoice(CategoryProduct product)
        {
            if (InvoiceProducts == null || InvoiceProducts.Count == 0)
            {
                AddProduct(product);
            }
            else
            {
                var checkProduct = InvoiceProducts.Find(ip => ip.Id == product.Id);
                if (checkProduct == null)
                {
                    AddProduct(product);
                }
                else
                {
                    checkProduct.Quantity += 1;
                    checkProduct.TotalPrice = checkProduct.Price * checkProduct.Quantity;
                }
            }
            UpdatePriceDetail();
            ReloadInvoiceProduct();
        }

        private void AddProduct(CategoryProduct product)
        {
            InvoiceProduct invoiceProduct = new InvoiceProduct();
            invoiceProduct.Id = product.Id;
            invoiceProduct.Name = product.Name;
            invoiceProduct.Quantity = 1;
            invoiceProduct.Price = product.Price;
            invoiceProduct.TotalPrice = invoiceProduct.Price * invoiceProduct.Quantity;

            InvoiceProducts.Add(invoiceProduct);
        }

        private void RemoveProductFromInvoice(InvoiceProduct invoiceProduct)
        {
            InvoiceProducts.Remove(invoiceProduct);
            ReloadInvoiceProduct();
            SelectedInvoiceProduct = null;
            UpdatePriceDetail();
        }

        private void ReloadInvoiceProduct()
        {
            var list = InvoiceProducts;
            InvoiceProducts = null;
            InvoiceProducts = list;

            SelectedProduct = null;
        }

        private double UpdateProductPrice()
        {
            if (InvoiceProducts == null || InvoiceProducts.Count == 0)
            {
                ProductPrice = 0;
                return 0;
            }
            double price = 0;
            foreach (var item in InvoiceProducts)
            {
                price += item.TotalPrice;
            }
            price = Math.Round(price, 2);
            ProductPrice = price;
            return price;
        }

        private void UpdatePriceDetail()
        {
            var productPrice = UpdateProductPrice();

            Tax = 0;

            TotalPrice = Math.Round(productPrice, 2);

        }

        private void ExecuteRemoveInvoiceProductCommand(object obj)
        {
            if (SelectedInvoiceProduct != null)
            {
                RemoveProductFromInvoice(SelectedInvoiceProduct);
            }
        }
        private async void ExecuteConfirmCreatingInvoiceCommand(object obj)
        {
            if (InvoiceProducts == null)
            {
                MessageBox.Show("Please add more items in invoice!");
                return;
            }
            else
            {
                try
                {
                    var invoice = GetConfirmInvoice();
                    var invoiceJsonObject = JsonConvert.SerializeObject(invoice);
                    var request = await GlobalVarible.BaseUrl
                        .AppendPathSegment("invoice")
                        .AllowAnyHttpStatus()
                        .WithOAuthBearerToken(GlobalVarible.Token)
                        .WithHeader("Content-Type", "application/json")
                        .PostStringAsync(invoiceJsonObject);

                    if (request.StatusCode == 200 || request.StatusCode == 201)
                    {
                        MessageBox.Show("Create Invoice Successfully!");
                        ExecuteCancelCreatingInvoiceCommand(null);
                    }
                    else
                    {
                        MessageBox.Show($"Create Invoice Fail!\nStatus Code: {request.ResponseMessage.ToString()}");
                    }
                }
                catch (FlurlHttpException ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }

        private void ExecuteCancelCreatingInvoiceCommand(object obj)
        {
            InvoiceProducts.Clear();
            ReloadInvoiceProduct();
            UpdatePriceDetail();
        }

        private Invoice GetConfirmInvoice()
        {
            Invoice invoice = new Invoice();
            invoice.Payment = (int)PaymentType.AtStore;
            invoice.VoucherId = 1;
            invoice.InvoiceDetails = new List<InvoiceDetail>();
            foreach (var item in InvoiceProducts)
            {
                InvoiceDetail invoiceDetail = new InvoiceDetail();
                invoiceDetail.ProductId = item.Id;
                invoiceDetail.Quantity = item.Quantity;
                invoiceDetail.Note = "";
                invoice.InvoiceDetails.Add(invoiceDetail);
            }
            return invoice;
        }
        private void ExecuteDecreaseProductCommand(object obj)
        {
            var product = SelectedInvoiceProduct;
            if (product != null)
            {
                product.Quantity--;
                if (product.Quantity == 0)
                {
                    RemoveProductFromInvoice(product);
                }
                else
                {
                    product.TotalPrice = product.Price * product.Quantity;
                    UpdatePriceDetail();
                    ReloadInvoiceProduct();
                }
            }
        }

        private void ExecuteIncreaseProductCommand(object obj)
        {
            var product = SelectedInvoiceProduct;
            if(product != null)
            {
                product.Quantity++;
                product.TotalPrice = product.Price * product.Quantity;

                UpdatePriceDetail();
                ReloadInvoiceProduct();
            }
        }
        #endregion
    }
}
