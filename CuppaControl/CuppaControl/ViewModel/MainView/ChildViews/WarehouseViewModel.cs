﻿using CuppaControl.Model;
using Flurl.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Windows.Input;
using CuppaControl.Global;
using Flurl;
using System.Threading.Tasks;
using System.Linq;
using System.Windows;

namespace CuppaControl.ViewModel
{
    public class WarehouseViewModel : ViewModelBase
    {
        #region Fields

        private int pageSize = 10;
        private int _currentPage;
        private int _totalPage;

        private bool _prevButtonEnable;
        private bool _nextButtonEnable;

        private List<string> _sortList;
        private int _sortSelectedIndex;

        private List<string> _statusList;
        private int _statusSelectedIndex;

        private string _searchKeyword;

        private List<ProductCategory> _categories;
        private ProductCategory _selectedCategory;

        private List<Good> _goods;
        private Good _selectedGood;

        private Visibility _notFoundVisibility;
        #endregion

        #region Properties
        public List<string> SortList
        {
            get
            {
                return _sortList;
            }
            set
            {
                _sortList = value;
                OnPropertyChanged(nameof(SortList));
            }
        }
        public int SortSelectedIndex
        {
            get
            {
                return _sortSelectedIndex;
            }
            set
            {
                _sortSelectedIndex = value;
                OnPropertyChanged(nameof(SortSelectedIndex));
            }
        }

        public List<string> StatusList
        {
            get
            {
                return _statusList;
            }
            set
            {
                _statusList = value;
                OnPropertyChanged(nameof(StatusList));
            }
        }
        public int StatusSelectedIndex
        {
            get
            {
                return _statusSelectedIndex;
            }
            set
            {
                _statusSelectedIndex = value;
                OnPropertyChanged(nameof(StatusSelectedIndex));
            }
        }

        public string SearchKeyword
        {
            get
            {
                return _searchKeyword;
            }
            set
            {
                _searchKeyword = value;
                OnPropertyChanged(nameof(SearchKeyword));
            }
        }


        public int CurrentPage
        {
            get
            {
                return _currentPage;
            }
            set
            {
                _currentPage = value;
                OnPropertyChanged(nameof(CurrentPage));
            }
        }
        public int TotalPage
        {
            get
            {
                return _totalPage;
            }
            set
            {
                _totalPage = value;
                OnPropertyChanged(nameof(TotalPage));
            }
        }
        public bool PrevButtonEnable
        {
            get
            {
                return _prevButtonEnable;
            }
            set
            {
                _prevButtonEnable = value;
                OnPropertyChanged(nameof(PrevButtonEnable));
            }
        }
        public bool NextButtonEnable
        {
            get
            {
                return _nextButtonEnable;
            }
            set
            {
                _nextButtonEnable = value;
                OnPropertyChanged(nameof(NextButtonEnable));
            }
        }

        public List<ProductCategory> Categories
        {
            get
            {
                return _categories;
            }
            set
            {
                _categories = value;
                OnPropertyChanged(nameof(Categories));
            }
        }

        public ProductCategory SelectedCategory
        {
            get
            {
                return _selectedCategory;
            }
            set
            {
                _selectedCategory = value;
                OnPropertyChanged(nameof(SelectedCategory));
            }
        }

        public List<Good> Goods
        {
            get
            {
                return _goods;
            }
            set
            {
                _goods = value;
                CheckPrevNextButtonEnable();
                OnPropertyChanged(nameof(Goods));
            }
        }
        public Good SelectedGood
        {
            get
            {
                return _selectedGood;
            }
            set
            {
                _selectedGood = value;
                OnPropertyChanged(nameof(SelectedGood));
            }
        }

        public Visibility NotFoundVisibility
        {
            get
            {
                return _notFoundVisibility;
            }
            set
            {
                _notFoundVisibility = value;
                OnPropertyChanged(nameof(NotFoundVisibility));
            }
        }
        #endregion

        #region Commands
        public ICommand SearchGoodsCommand { get; }
        public ICommand NextGoodPageCommand { get; }
        public ICommand PrevGoodPageCommand { get; }
        #endregion

        #region Constructors
        public WarehouseViewModel()
        {
            SortList = new List<string>() { "Default", "Name ASC", "Name DESC", "Quantity ASC", "Quantity DESC" };
            StatusList = new List<string>() { "All", "In use", "Not in use" };
            GetGoodCategoryList();
            GetInitialGoods();

            SearchGoodsCommand = new ViewModelCommand(ExecuteSearchGoodsCommand);
            NextGoodPageCommand = new ViewModelCommand(ExecuteNextGoodPageCommand);
            PrevGoodPageCommand = new ViewModelCommand(ExecutePrevGoodPageCommand);
        }

        #endregion

        #region Methods
        private void ExecutePrevGoodPageCommand(object obj)
        {
            CurrentPage--;
            LoadGoodPage();
        }

        private void ExecuteNextGoodPageCommand(object obj)
        {
            CurrentPage++;
            LoadGoodPage();
        }

        private void ExecuteSearchGoodsCommand(object obj)
        {
            SearchGoods();
        }

        private void CheckPrevNextButtonEnable()
        {
            if (Goods == null || Goods.Count == 0)
            {
                PrevButtonEnable = false;
                NextButtonEnable = false;
                return;
            }

            // Prev Button
            if (CurrentPage == 1 ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                PrevButtonEnable = false;
            }
            else if (CurrentPage > 1 && CurrentPage <= TotalPage)
            {
                PrevButtonEnable = true;
            }
            else
            {
                PrevButtonEnable = false;
            }

            // Next Button
            if (CurrentPage == TotalPage ||
                (CurrentPage == 1) && (TotalPage == 1))
            {
                NextButtonEnable = false;
            }
            else if (CurrentPage >= 1 && CurrentPage < TotalPage)
            {
                NextButtonEnable = true;
            }
            else
            {
                NextButtonEnable = false;
            }

        }

        private async void GetInitialGoods()
        {
            try
            {

                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("inventory")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<Good> goodList = JsonConvert.DeserializeObject<List<Good>>(result["result"]["list"].ToString());
                    foreach (var item in goodList)
                    {
                        if(item.IsInUser == "True")
                        {
                            item.IsInUser = "Yes";
                        }
                        else
                        {
                            item.IsInUser = "No";
                        }
                    }
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    CurrentPage = 1;
                    NotFoundVisibility = Visibility.Collapsed;
                    Goods = goodList;
                    SelectedGood = null;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void GetGoodCategoryList()
        {
            try
            {
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("inventory")
                    .AppendPathSegment("category")
                    .AppendPathSegment("list")
                    .AllowAnyHttpStatus()
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    List<ProductCategory> cateList = JsonConvert.DeserializeObject<List<ProductCategory>>(result["result"].ToString());
                    ProductCategory cate = new ProductCategory() { Id = 0, Name = "All" };
                    cateList.Add(cate);
                    Categories = cateList;
                    SelectedCategory = Categories[Categories.Count - 1];
                }
                else
                {

                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private void RefreshGoods()
        {
            Goods = new List<Good>();
            SelectedGood = null;
        }

        private string GetSortName()
        {
            switch (SortSelectedIndex)
            {
                case 1:
                    return "asc";
                case 2:
                    return "desc";
                default:
                    return string.Empty;
            }
        }
        private string GetSortQuantity()
        {
            switch (SortSelectedIndex)
            {
                case 3:
                    return "asc";
                case 4:
                    return "desc";
                default:
                    return string.Empty;
            }
        }
        private string GetStatus()
        {
            switch (StatusSelectedIndex)
            {
                case 1:
                    return "TRUE";
                case 2:
                    return "FALSE";
                default:
                    return string.Empty;
            }
        }

        private async void LoadGoodPage()
        {
            try
            {
                RefreshGoods();
                ProductCategory cate = Categories.Find(c => c.Id == SelectedCategory.Id);
                string cateName = (cate == null || cate.Id == 0) ? string.Empty : cate.Name;
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("inventory")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("Name", SearchKeyword)
                    .SetQueryParam("Category", cateName)
                    .SetQueryParam("isInUser", GetStatus())
                    .SetQueryParam("sortName", GetSortName())
                    .SetQueryParam("sortQuantity", GetSortQuantity())
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", CurrentPage)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<Good> goodList = JsonConvert.DeserializeObject<List<Good>>(result["result"]["list"].ToString());
                    foreach (var item in goodList)
                    {
                        if (item.IsInUser == "True")
                        {
                            item.IsInUser = "Yes";
                        }
                        else
                        {
                            item.IsInUser = "No";
                        }
                    }
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    NotFoundVisibility = Visibility.Collapsed;
                    Goods = goodList;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private async void SearchGoods()
        {
            try
            {
                RefreshGoods();
                ProductCategory cate = Categories.Find(c => c.Id == SelectedCategory.Id);
                string cateName = (cate == null || cate.Id == 0) ? string.Empty : cate.Name;
                var response = await GlobalVarible.BaseUrl
                    .AppendPathSegment("inventory")
                    .AppendPathSegment("filter")
                    .AllowAnyHttpStatus()
                    .SetQueryParam("Name", SearchKeyword)
                    .SetQueryParam("Category", cateName)
                    .SetQueryParam("isInUser", GetStatus())
                    .SetQueryParam("sortName", GetSortName())
                    .SetQueryParam("sortQuantity", GetSortQuantity())
                    .SetQueryParam("pageSize", pageSize)
                    .SetQueryParam("pageNumber", 1)
                    .GetAsync();
                if (response.StatusCode == 200)
                {
                    var result = JObject.Parse(await response.ResponseMessage.Content.ReadAsStringAsync());
                    //var checkResult = JObject.Parse(result["result"].ToString());
                    if (!result["result"].HasValues)
                    {
                        NotFoundVisibility = Visibility.Visible;
                        ResetPages();
                        CheckPrevNextButtonEnable();
                        return;
                    }
                    List<Good> goodList = JsonConvert.DeserializeObject<List<Good>>(result["result"]["list"].ToString());
                    foreach (var item in goodList)
                    {
                        if (item.IsInUser == "True")
                        {
                            item.IsInUser = "Yes";
                        }
                        else
                        {
                            item.IsInUser = "No";
                        }
                    }
                    int totalPage = JsonConvert.DeserializeObject<int>(result["result"]["totalPage"].ToString());
                    TotalPage = totalPage;
                    CurrentPage = 1;
                    NotFoundVisibility = Visibility.Collapsed;
                    Goods = goodList;
                }
                else
                {
                    NotFoundVisibility = Visibility.Visible;
                    ResetPages();
                    CheckPrevNextButtonEnable();
                }
            }
            catch (FlurlHttpException)
            {

            }
        }

        private void ResetPages()
        {
            TotalPage = 0;
            CurrentPage = 0;
        }
        #endregion
    }
}
