**Cuppa Control WPF Application**

Please change the BaseURL in the GlobalVarible class to consume the API (/Global/GlobalVarible.cs).

The BaseURL should be change to the local host port of the Cuppa Control Web API application on each device.
EX: http://localhost:0000/api, replace the 0000 with the number of your device port.

The Web API is not deployed to a server yet.
